#!/bin/bash

# This script is to test the malware detection capabilities of an Intrusion
# Detection System (IDS).
#
# It downloads a list of malware URLs and sends requests to them using random user agents.
# The script will continuously download and request URLs, and sends the output to
# /dev/null to avoid storing any actual malware on the system.

agent_url='https://jnrbsn.github.io/user-agents/user-agents.json'
malware_url='https://urlhaus.abuse.ch/downloads/json_online/'
pkgs='jq'
n_urls=0

# Check if the script is run as root
if [ "$(id -u)" -ne 0 ]; then
    printf "\033[31mPlease run this script as root.\033[0m\n"
    exit 1
fi

# Trap Ctrl+C to print a message before exiting
trap 'printf "\r\033[33mDownloaded \033[34m$n_urls\033[33m URL(s).\033[0m\n" && exit' INT

# Make sure jq and wget are installed
if ! command -v jq >/dev/null 2>&1 || ! command -v wget >/dev/null 2>&1; then
    printf "\033[33mRequired tools are missing. Attempting to install...\033[0m\n"

    # Initialise a flag to track installation success
    installed=0

    # List of package managers
    for pm in apt apt-get dnf yum zypper pacman apk emerge swupd; do
        if command -v $pm >/dev/null 2>&1; then
            case "$pm" in
            apt | apt-get)
                cmd="$pm update >/dev/null 2>&1 && $pm install -y $pkgs >/dev/null 2>&1"
                ;;
            dnf | yum | zypper)
                cmd="$pm install -y $pkgs >/dev/null 2>&1"
                ;;
            pacman)
                cmd="$pm -Sy --noconfirm $pkgs >/dev/null 2>&1"
                ;;
            apk)
                cmd="$pm add $pkgs >/dev/null 2>&1"
                ;;
            emerge)
                cmd="$pm --sync >/dev/null 2>&1 && $pm --verbose $pkgs >/dev/null 2>&1"
                ;;
            swupd)
                cmd="$pm update >/dev/null 2>&1 && $pm bundle-add $pkgs >/dev/null 2>&1"
                ;;
            esac

            # Run the package manager command
            if eval "$cmd"; then
                installed=1
                break
            fi
        fi
    done

    # Erase the previous line to avoid cluttering the output
    printf "\033[A\033[2K"

    # If installation failed
    if [ $installed -ne 1 ]; then
        printf "\033[31mFailed to install required packages. Exiting.\033[0m\n"
        exit 1
    fi
fi

# Download the user agents from the URL and store them in a string
user_agents=$(wget -qO- "$agent_url" | jq -r '.[]')

# Download the malware URLs from the URL and store them in a string
url_string=$(wget -qO- "$malware_url" | jq -r '.. | .url? // empty')

# Check if the malware URL data was downloaded successfully, exit if not
if [ -z "$url_string" ]; then
    printf "\033[31mFailed to download malware URLs. Exiting.\033[0m\n"
    exit 1
fi

printf "\033[94mDownloading malware URLs to /dev/null. Do \033[1;4mNOT\033[22;24m manually download or execute any files.\033[0m\nHit Ctrl+C to exit.\n\n"

while true; do
    # Get a random user agent
    user_agent=$(echo "$user_agents" | shuf -n 1)

    # Get a random malware url
    url=$(echo "$url_string" | shuf -n 1)

    # Print the URL
    printf "Downloading \033[31m%b\033[0m\n" "$url"

    # Perform the request
    wget -q --user-agent="$user_agent" "$url" -O /dev/null --timeout=3 >/dev/null 2>&1
    n_urls=$((n_urls + 1))
done
