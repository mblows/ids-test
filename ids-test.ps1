#!/usr/bin/env pwsh

# This script is to test the malware detection capabilities of an Intrusion
# Detection System (IDS).
#
# It downloads a list of malware URLs and sends requests to them using random user agents.
# The script will continuously download and request URLs, and sends the output to
# the NULL device to avoid storing any actual malware on the system.

$agent_url = 'https://jnrbsn.github.io/user-agents/user-agents.json'
$malware_url = 'https://urlhaus.abuse.ch/downloads/json_online/'

# Disable progress bar output for download requests
$ProgressPreference = 'SilentlyContinue'

# Download the user agents from the URL and store them in an array
$user_agents = (Invoke-WebRequest -Uri $agent_url).Content | ConvertFrom-Json

# Download the malware URLs from the URL and store them in an array
$url_array = (Invoke-WebRequest -Uri $malware_url).Content | ConvertFrom-Json | ForEach-Object { $_.PsObject.Properties.Value.url }

# Check if the malware URL data was downloaded successfully, exit if not
if ($url_array.Count -eq 0) {
    Write-Host "Failed to download malware URLs. Exiting."
    exit
}

Write-Host "Downloading malware URLs to NULL. Do NOT manually download or execute any files.`nHit Ctrl+C to exit `n"

while ($true) {
    # Get a random user agent
    $user_agent = Get-Random -InputObject $user_agents

    # Get a random URL
    $url = Get-Random -InputObject $url_array

    # Print the URL
    Write-Host "Downloading $url"

    # Perform the request
    try {
        Invoke-WebRequest -Uri $url -UserAgent $user_agent -TimeoutSec 3 | Out-Null
    }
    catch {
        continue
    }
}

