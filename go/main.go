package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"runtime"
	"syscall"
	"time"
	"unicode/utf8"

	"github.com/schollz/progressbar/v3"
	"golang.org/x/term"
)

var (
	ua_url      = "https://jnrbsn.github.io/user-agents/user-agents.json"
	malware_url = "https://urlhaus.abuse.ch/downloads/json_online/"

	client = http.Client{
		Timeout: 15 * time.Second,
	}
)

type URLInfo struct {
	DateAdded   string   `json:"dateadded"`
	URL         string   `json:"url"`
	URLStatus   string   `json:"url_status"`
	LastOnline  string   `json:"last_online"`
	Threat      string   `json:"threat"`
	Tags        []string `json:"tags"`
	URLHausLink string   `json:"urlhaus_link"`
	Reporter    string   `json:"reporter"`
}

type URLData map[string][]URLInfo

// stripANSI removes ANSI escape sequences from a string
func stripANSI(input string) string {
	ansiEscapeRegex := regexp.MustCompile(`(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]`)
	return ansiEscapeRegex.ReplaceAllString(input, "")
}

// truncateText truncates a string to a specified length
func truncateText(text string, length int) string {
	cleanText := stripANSI(text)
	if utf8.RuneCountInString(cleanText) > length {
		runes := []rune(cleanText)
		return string(runes[:length-3]) + "..."
	}
	return cleanText
}

// download downloads a file from a URL with a specified user agent.
// If showProgress is true, it displays a progress bar.
func download(url, agent string, showProgress bool) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("creating request: %w", err)
	}

	if agent != "" {
		req.Header.Set("User-Agent", agent)
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("executing request: %w", err)
	}
	defer resp.Body.Close()

	buf := new(bytes.Buffer)
	var reader io.Reader = resp.Body

	if showProgress && resp.ContentLength > 0 {
		width, _, _ := term.GetSize(int(os.Stdout.Fd()))
		bar := progressbar.NewOptions64(
			resp.ContentLength,
			progressbar.OptionSetDescription(fmt.Sprintf("\033[34m%s\033[0m", truncateText(url, width/2))),
			progressbar.OptionSetWriter(os.Stdout),
			progressbar.OptionEnableColorCodes(true),
			progressbar.OptionShowBytes(true),
			progressbar.OptionSetWidth(10),
			progressbar.OptionFullWidth(),
			progressbar.OptionUseIECUnits(true),
			progressbar.OptionSpinnerType(14),
			progressbar.OptionSetRenderBlankState(true),
			progressbar.OptionOnCompletion(func() { fmt.Fprint(os.Stdout, "\n") }),
			progressbar.OptionSetTheme(progressbar.Theme{
				Saucer:        "\033[31m━\033[0m",
				SaucerPadding: "━",
			}),
		)
		reader = io.TeeReader(resp.Body, bar)
	}

	_, err = io.Copy(buf, reader)
	if err != nil {
		return nil, fmt.Errorf("copying data: %w", err)
	}

	return buf.Bytes(), nil
}

// getUserAgents gets a list of user agents from a JSON file
func getUserAgents() ([]string, error) {
	data, err := download(ua_url, "", false)
	if err != nil {
		return nil, err
	}

	var userAgents []string
	err = json.Unmarshal(data, &userAgents)
	if err != nil {
		return nil, err
	}

	return userAgents, nil
}

// getMalwareURLs gets a list of malware URLs from a URL
func getMalwareURLs(url, agent string) ([]string, error) {
	data, err := download(url, agent, false)
	if err != nil {
		return nil, err
	}

	var urlData URLData
	err = json.Unmarshal(data, &urlData)
	if err != nil {
		return nil, err
	}

	var urls []string
	for _, info := range urlData {
		urls = append(urls, info[0].URL)
	}

	return urls, nil
}

func main() {
	// Enable virtual terminal processing on Windows
	enableVirtualTerminalProcessing()

	n_urls := 0

	// Set the null device to /dev/null on Unix-like systems, and NUL on Windows
	null_device := "NUL"
	if runtime.GOOS != "windows" {
		null_device = "/dev/null"
	}

	// Create a channel to handle SIGINT
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT)

	// Exit gracefully on SIGINT
	go func() {
		<-sigCh
		fmt.Printf("\033[2K\nDownloaded %d malware URL(s).\n", n_urls)
		os.Exit(0)
	}()

	fmt.Printf("Downloading malware URLs to %s. Do \033[4;1mNOT\033[0m manually download or execute any files.\nHit Ctrl+C to exit.\n\n", null_device)

	// Get a list of random user agents
	user_agents, err := getUserAgents()
	if err != nil {
		fmt.Println("Error getting user agents:", err)
		return
	}

	// Get a list of malware URLs using a random user agent
	urls, err := getMalwareURLs(malware_url, user_agents[rand.Intn(len(user_agents))])
	if err != nil {
		fmt.Println("Error getting malware URLs:", err)
		return
	}

	// Download the malware URLs with a progress bar, again using a random user agent
	for _, url := range urls {
		_, err := download(url, user_agents[rand.Intn(len(user_agents))], true)
		if err != nil {
			continue
		}
		n_urls++
	}
}
