module gitlab.com/mblows/ids-test

go 1.23

require github.com/schollz/progressbar/v3 v3.17.0

require (
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	golang.org/x/sys v0.26.0
	golang.org/x/term v0.25.0
)
