//go:build !windows
// +build !windows

package main

// enableVirtualTerminalProcessing enables virtual terminal processing on Windows,
// but does nothing on other operating systems.
func enableVirtualTerminalProcessing() {}
