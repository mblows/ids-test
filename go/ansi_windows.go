//go:build windows
// +build windows

package main

import (
	"os"

	"golang.org/x/sys/windows"
)

// enableVirtualTerminalProcessing enables virtual terminal processing on Windows
func enableVirtualTerminalProcessing() {
	// Get the handle to the console output
	handle := windows.Handle(os.Stdout.Fd())

	// Get the current console mode
	var mode uint32
	windows.GetConsoleMode(handle, &mode)

	// Enable virtual terminal processing
	mode |= windows.ENABLE_VIRTUAL_TERMINAL_PROCESSING
	windows.SetConsoleMode(handle, mode)
}
