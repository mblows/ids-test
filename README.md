# IDS Malware Detection Scripts

These scripts are used to trigger alerts in an IDS/IPS like Snort or Suricata. They simply connect to the Urlhaus API and download the latest malware URLs with a different user agent each time. The actual output of the downloads is redirected to `/dev/null` (`NUL` on Windows) to avoid actually downloading the malware.

At this stage, there is a [shell script for Linux](https://gitlab.com/mblows/ids-test/-/blob/main/ids-test.sh) (or WSL), a [PowerShell script for Windows](https://gitlab.com/mblows/ids-test/-/blob/main/ids-test.ps1), and Windows and Linux binaries.

After inspecting the scripts (it's not a good idea to blindly run random scripts), you can either download them onto a machine in your LAN and run them directly, or combine downloading and running them in a single command.

For example, to download and run the Linux script, you can use the following command:

```shell
sh -c "$(wget https://gitlab.com/mblows/ids-test/-/raw/main/ids-test.sh -qO-)"
```

and on Windows:

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-RestMethod "https://gitlab.com/mblows/ids-test/-/raw/main/ids-test.ps1" | Invoke-Expression
```

It's important to note that these are **real** malware URLs, so do not download or run them manually.

If you'd prefer to be extremely safe, you can also run them in a Docker container:

```shell
docker run --rm -it alpine:latest \
sh -c "$(wget https://gitlab.com/mblows/ids-test/-/raw/main/ids-test.sh -qO-)"
```

This downloads the [Alpine Linux](https://www.alpinelinux.org/) container (only ~5MiB), and runs the script inside it instead.

## Binary Releases

If you cannot for some reason run the scripts above, you can download Linux and Windows binaries from the [releases page](https://gitlab.com/mblows/ids-test/-/releases/permalink/latest).
